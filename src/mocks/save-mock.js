import MockAdapter from "axios-mock-adapter";
export const startMock = (axios) => {

    const mock = new MockAdapter(axios, {
        delayResponse: 1000,
        onNoMatch: 'passthrough'
    });
    mock.onPost("/send-personal-info").reply(
        function (config) {
            return [
                200, {
                    "status": true
                }
            ]
    });
    mock.onPost("/send-address-info").reply(
        function (config) {
            return [
                200, {
                    "status": true
                }
            ]
    });
    mock.onPost("/send-preferences-info").reply(
        function (config) {
            return [
                200, {
                    "status": true
                }
            ]
    });
};