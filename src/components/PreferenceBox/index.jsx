import React, { useState } from "react";
import checkboxEmptyIcon from "../../images/order-details/checkbox-empty.svg";
import checkboxFilledIcon from "../../images/order-details/checkbox-filled.svg";
import styles from './index.module.scss';

const PreferenceBox = ({ preferences, text, modifyPreferences }) => {
  const isFilled = () => {
    return preferences.includes(text);
  };

  const handleClick = () => {
    let list = [...preferences];
    if (isFilled()) {
      let newList = list.filter((each) => each !== text);
      modifyPreferences([...newList]);
    } else {
      list.push(text);
      modifyPreferences([...list]);
    }
  };
  return (
    <div className={styles.preference_} onClick={handleClick}>
      <img src={isFilled() ? checkboxFilledIcon : checkboxEmptyIcon} alt="" />
      <div>{text}</div>
    </div>
  );
};

export default PreferenceBox;
