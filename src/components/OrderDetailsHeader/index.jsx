import React, { useEffect } from 'react';
import styles from './index.module.scss';

const OrderDetailsHeader = () => {
  return (
    <div className={styles.OrderDetailsHeader_}>
        <div className={styles.inner}>
            <h1>Order Details</h1>
            <div>Ready to join the world’s most ambitious luxury marketplace?</div>
        </div>
    </div>
  )
}

export default OrderDetailsHeader