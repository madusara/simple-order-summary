import React, { useEffect } from "react";
import styles from "./index.module.scss";
import logo from "../../images/navbar/logo.svg";
import userIcon from "../../images/navbar/user-icon.svg";
import cartIcon from "../../images/navbar/cart-icon.svg";
import magnifyIcon from "../../images/navbar/magnify-icon.svg";

const SiteNavbar = () => {
  // useEffect(() => {
  //   window.addEventListener('scroll', myFunction())
  //   return () => {
  //     window.removeEventListener('scroll', myFunction())
  //   }
  // }, [])
  
  // const myFunction = () => {
  //   var header = document.getElementById("myHeader");
  //   if (window.pageYOffset > header.offsetTop) {
  //     header.classList.add("sticky");
  //   } else {
  //     header.classList.remove("sticky");
  //   }
  // }
  return (
    <div className={styles.siteNavbar_} id="myHeader">
      <div className={styles.inner}>
        <div className={styles.leftLinks}>
          <a href="/">Buy</a>
          <a href="/">Sell</a>
          <a href="/">Invest</a>
          <a href="/">
            <img src={magnifyIcon} alt="" />
            Search
          </a>
        </div>
        <a href="/" className={styles.mainLogo}>
          <img src={logo} alt="" />
        </a>
        <div className={styles.rightLinks}>
          <a href="/">How It Works</a>
          <a href="/">The Lymited Journal</a>
          <a href="/" className={styles.registerLink}>
            Register
          </a>
          <a href="/" className={styles.userLink}>
            <img src={userIcon} alt="" />
          </a>
          <a href="/">
            <img src={cartIcon} alt="" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default SiteNavbar;
