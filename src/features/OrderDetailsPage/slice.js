import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { saveMock } from "../../mocks";

export const initialState = {
    isLoading: false,
    isPersonalInfoSentSuccessfully: false
};

saveMock.startMock(axios);

export const savePersonalInfo = createAsyncThunk(
    "orderDetails/savePersonalInfo",
    async (personalData) => {
        await axios
            .post("/send-personal-info", { personalData })
            .then((res) => {
                console.log("Personal info api call was successful")
            })
            .catch((err) => console.log(err));
        return true;
    }
);

export const saveAddressInfo = createAsyncThunk(
    "orderDetails/saveAddressInfo",
    async (addressData) => {
        await axios
            .post("/send-address-info", { addressData })
            .then((res) => {
                console.log("Address info api call was successful")
            })
            .catch((err) => console.log(err));
        return true;
    }
);

export const savePreferencesInfo = createAsyncThunk(
    "orderDetails/savePreferencesInfo",
    async (preferencesData) => {
        await axios
            .post("/send-preferences-info", { preferencesData })
            .then((res) => {
                console.log("Preferences info api call was successful")
            })
            .catch((err) => console.log(err));
        return true;
    }
);

const sheetsSlice = createSlice({
    name: "orderDetails",
    initialState,
    extraReducers: {
        // I wanted to demonstrate how thunk works here with the use of a loading state. Of course it won't work as we don't have an api
        [savePersonalInfo.pending]: (state, action) => {
            state.isLoading = true;
        },
        [savePersonalInfo.fulfilled]: (state, action) => {
            console.log("fulfilled personal info - extraReducer");
            state.isPersonalInfoSentSuccessfully = true;
            state.isLoading = false;
        },
        [savePersonalInfo.rejected]: (state, action) => {
            state.isLoading = false;
        },

        [saveAddressInfo.pending]: (state, action) => {
            state.isLoading = true;
        },
        [saveAddressInfo.fulfilled]: (state, action) => {
            console.log("fulfilled address info - extraReducer");
            state.isPersonalInfoSentSuccessfully = false;
            state.isLoading = false;
        },
        [saveAddressInfo.rejected]: (state, action) => {
            state.isLoading = false;
        },

        [savePreferencesInfo.pending]: (state, action) => {
            state.isLoading = true;
        },
        [savePreferencesInfo.fulfilled]: (state, action) => {
            console.log("fulfilled preference info - extraReducer");
            state.isPersonalInfoSentSuccessfully = false;
            state.isLoading = false;
        },
        [savePreferencesInfo.rejected]: (state, action) => {
            state.isLoading = false;
        },
    },
});

export default sheetsSlice.reducer;
