import React, { useState, useEffect, useRef } from "react";
import Select from "react-select";
import AsyncSelect from "react-select/async";
import axios from "axios";
import SimpleReactValidator from "simple-react-validator";
import OrderDetailsHeader from "../../components/OrderDetailsHeader";
import SiteNavbar from "../../components/SiteNavbar";
import { customSelectStyles } from "./reactSelectStyles";
import { useDispatch, useSelector } from "react-redux";
import styles from "./index.module.scss";
import "./index.scss";
import PreferenceBox from "../../components/PreferenceBox";
import slashIcon from "../../images/order-details/slash.svg";
import { savePersonalInfo, saveAddressInfo, savePreferencesInfo } from "./slice";

const OrderDetails = () => {
  const [countriesList, setCountries] = useState([]);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [chosenCountry, setChosenCountry] = useState("");
  const [address1, setAddress1] = useState("");
  const [address2, setAddress2] = useState("");
  const [addressCountry, setAddressCountry] = useState("");
  const [addressCity, setAddressCity] = useState("");
  const [addressPostalCode, setAddressPostalCode] = useState("");
  const [preferences, setPreferences] = useState([]);

  const preferenceOptions = ["Cars", "Books", "Watches", "Laptops"];

  const [, forceUpdate] = useState(0); // used in the validation part

  const dispatch = useDispatch();
  const isPersonalInfoSentSuccessfully = useSelector(
    (state) => state.orderDetails.isPersonalInfoSentSuccessfully
  );

  // define a validator instance
  const simpleValidator = useRef(
    new SimpleReactValidator({
      messages: {
        required: "Required",
      },
    })
  );

  // fetch countries list at the component mount
  useEffect(() => {
    let countryItem = {};
    let countriesList = [];
    axios.get("https://restcountries.com/v2/all").then((result) => {
      result.data.map((country) => {
        countryItem = {
          label: country.name,
          value: country.alpha2Code,
        };
        countriesList.push(countryItem);
      });
      setCountries([...countriesList]);
    });
  }, []);

  const loadOptions = (inputValue, callBack) => {
    let resultsList = [];

    // if a country is chosen, we'll add that as a filter to the address search
    let countryFilter = chosenCountry
      ? `&filter=countrycode:${chosenCountry.toLowerCase()}`
      : "";

    var config = {
      method: "get",
      url: `https://api.geoapify.com/v1/geocode/autocomplete?text=${inputValue}${countryFilter}&apiKey=3f3b396c546d47aa88d993ab198656bc`,
      headers: {},
    };

    if (inputValue) {
      axios(config)
        .then(function (response) {
          let formattedResult = {};
          response.data.features.map((result) => {
            formattedResult = {
              label: result.properties.formatted,
              value:
                "517a39ecbe6333534059946ea69df52a3840f00103f901e5061dea01000000c0020892030954616c616920426568",
              address1: result.properties.address_line1,
              address2: result.properties.address_line2,
              cityTown: result.properties.city,
              country: result.properties.country,
              postalCode: result.properties.postcode,
            };
            resultsList.push(formattedResult);
          });

          // set the options array
          callBack([...resultsList]);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  const onAddressSelect = (value) => {
    // when a user clicks on a suggested address, this function will populate the relevant fields 
    if (value) {
      setAddress1(value.address1);
      setAddress2(value.address2);
      setAddressCountry(value.country);
      setAddressCity(value.cityTown);
      setAddressPostalCode(value.postalCode ? value.postalCode : "");
    }
  };

  const onCountrySelect = (country) => {
    if (country) {
      setChosenCountry(country.value);
    }
  };

  const modifyPreferences = (newList) => {
    setPreferences([...newList]);
  };

  const handleSaveData = () => {
    // if all the fields are valid then we'll go to the next step, probably a dispatch
    // if (simpleValidator.current.allValid()) {
    const personalData = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      phone: phone,
    };

    //we are sending personal information only. Once it is successfully sent, well perform a useEffect to send the address and preferences
    dispatch(savePersonalInfo(personalData));

    // if some of the fields are not valid we''ll show the relavant error messages
    // } else {
    //   simpleValidator.current.showMessages();
    //   forceUpdate();
    // }
  };

  useEffect(() => {
    // this useEffect will listen to the changes of redux state 'isPersonalInfoSentSuccessfully'
    // and dispatch the actions to send address and preferences info
    const addressData = {
      address1: address1,
      address2: address2,
      city: addressCity,
      country: addressCountry,
      postalCode: addressPostalCode,
    };
    if(isPersonalInfoSentSuccessfully){
      dispatch(saveAddressInfo(addressData));

      // action to send preferences will be dispatched only if preferences are updated
      if(preferences.length > 0) dispatch(savePreferencesInfo(preferences));
    }
  }, [isPersonalInfoSentSuccessfully]);

  return (
    <div className={styles.mockup}>
      <div className={styles.onboardingBanner}>
        <SiteNavbar />
      </div>
      <OrderDetailsHeader />
      <div className={styles.yourDetailsWrap}>
        <div className={styles.inner}>
          <h4>YOUR DETAILS</h4>
          <div className={styles.inputs2row}>
            <div className={styles.inputWrapper}>
              <input
                type="text"
                placeholder="First Name*"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
              <div className={styles.validationError}>
                {simpleValidator.current.message(
                  "First name",
                  firstName,
                  "required|alpha"
                )}
              </div>
            </div>
            <div className={styles.inputWrapper}>
              <input
                type="text"
                placeholder="Last Name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
              <div className={styles.validationError}>
                {simpleValidator.current.message(
                  "title",
                  lastName,
                  "required|alpha"
                )}
              </div>
            </div>
          </div>
          <div className={styles.inputs2row}>
            <div className={styles.inputWrapper}>
              <input
                type="text"
                placeholder="Email Address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <div className={styles.validationError}>
                {simpleValidator.current.message(
                  "email",
                  email,
                  "required|email"
                )}
              </div>
            </div>
            <div className={styles.inputWrapper}>
              <input
                type="text"
                placeholder="Telephone Number"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
              <div className={styles.validationError}>
                {simpleValidator.current.message(
                  "Phone number",
                  phone,
                  "required|phone"
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.yourAddressWrap}>
        <div className={styles.inner}>
          <h4>YOUR ADDRESS</h4>
          <div className={styles.countryRow}>
            <Select
              className="customSelect"
              classNamePrefix="customSelect"
              placeholder="Country"
              isDisabled={false}
              isLoading={false}
              isClearable={false}
              isRtl={false}
              isSearchable={true}
              name="countrySelect"
              onChange={onCountrySelect}
              options={countriesList}
              styles={customSelectStyles}
            />
          </div>
          <div className={styles.searchAddressRow}>
            <AsyncSelect
              className="customSelect"
              classNamePrefix="customSelect"
              defaultOptions
              isSearchable={true}
              name="addressSelect"
              onChange={onAddressSelect}
              loadOptions={loadOptions}
              styles={customSelectStyles}
              placeholder='Type part of an address or postcode EG: "AM5 6QH" or "84 Sir Matt Busby Way'
            />
          </div>
          <div className={styles.addressLinesRow}>
            <div className={styles.inputWrapper}>
              <input
                type="text"
                placeholder="Address Line 1"
                value={address1}
                onChange={(e) => setAddress1(e.target.value)}
              />
              <div className={styles.validationError}>
                {simpleValidator.current.message(
                  "Address Line 1",
                  address1,
                  "required"
                )}
              </div>
            </div>
            <div className={styles.inputWrapper}>
              <input
                type="text"
                placeholder="Address Line 2"
                value={address2}
                onChange={(e) => setAddress2(e.target.value)}
              />
            </div>
          </div>
          <div className={styles.townOrStateRow}>
            <div className={styles.inputWrapper}>
              <input
                type="text"
                placeholder="Town/City"
                value={addressCity}
                onChange={(e) => setAddressCity(e.target.value)}
              />
              <div className={styles.validationError}>
                {simpleValidator.current.message(
                  "Town/City",
                  addressCity,
                  "required"
                )}
              </div>
            </div>
            <div className={styles.inputWrapper}>
              <input
                type="text"
                placeholder="County/Province/State"
                value={addressCountry}
                onChange={(e) => setAddressCountry(e.target.value)}
              />
              <div className={styles.validationError}>
                {simpleValidator.current.message(
                  "Country",
                  addressCountry,
                  "required"
                )}
              </div>
            </div>
          </div>
          <div className={styles.postalCodeRow}>
            <div className={styles.inputWrapper}>
              <input
                type="text"
                placeholder="Postal Code"
                value={addressPostalCode}
                onChange={(e) => setAddressPostalCode(e.target.value)}
              />
              <div className={styles.validationError}>
                {simpleValidator.current.message(
                  "Postal Code",
                  addressPostalCode,
                  "required"
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.yourPreferencesWrap}>
        <div className={styles.inner}>
          <h4>YOUR DETAILS</h4>
          <div className={styles.preferencesAndButtonWrap}>
            <div className={styles.leftPreferences}>
              <div className={styles.title}>I’d like to hear more about</div>
              <div className={styles.listOfPreferences}>
                {preferenceOptions.map((each, index) => {
                  return (
                    <div key={index} className={styles.eachPreference}>
                      <PreferenceBox
                        text={each}
                        preferences={preferences}
                        modifyPreferences={(list) => modifyPreferences(list)}
                      />
                    </div>
                  );
                })}
              </div>
            </div>
            <img src={slashIcon} alt="" className={styles.slashIcon} />
            <button
              className={styles.saveButton}
              onClick={() => handleSaveData()}
            >
              SAVE
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderDetails;
