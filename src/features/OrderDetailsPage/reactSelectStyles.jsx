export const customSelectStyles = {
  control: (provided, state) => ({
    ...provided,
    width: "100%",
    backgroundColor: "black",
    padding: "0.65rem 0",
    color: "white",
    border: state.isFocused ? "none" : "none",
    borderBottom: state.isFocused ? "1px solid #4BD7DD" : "1px solid #808080",
    borderColor: state.isFocused ? "#4BD7DD !important" : "#808080",
    boxShadow: "none",
    outline: "none",
    minHeight: "unset",
    borderRadius: "0",
    font: "400 1.4rem/1.8rem Montserrat, sans-serif",
  }),
  input: (provided) => ({
    ...provided,
    color: "white",
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = "opacity 300ms";
    const color = "white";
    return { ...provided, opacity, transition, color };
  },
  indicatorSeparator: () => ({
    display: "none",
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    padding: "0 8px",
  }),
  loadingIndicator: (provided) => ({
    ...provided,
    display: "none",
  }),
  menu: (provided) => ({
    ...provided,
    width: "100%",
    marginTop: "0",
  }),
  menuList: (provided) => ({
    ...provided,
    backgroundColor: "black",
  }),
  option: (provided, state) => ({
    ...provided,
    color: "white",
    backgroundColor: state.isSelected ? "#333" : "black",
    padding: "1.3rem 2rem",
    font: state.isSelected
      ? "600 1.4rem/1.8rem Montserrat, sans-serif"
      : "400 1.4rem/1.8rem Montserrat, sans-serif",
  }),
};
