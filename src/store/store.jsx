import { configureStore } from "@reduxjs/toolkit";
import orderDetailsReducer from '../features/OrderDetailsPage/slice'

export default configureStore({
    reducer: {
        orderDetails: orderDetailsReducer,
    },
})